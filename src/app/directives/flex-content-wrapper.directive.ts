import { AfterViewInit, Directive, ElementRef, ViewChild } from '@angular/core';

@Directive({
  selector: '[flexContentWrapper]',
})
export class FlexContentWrapperDirective implements AfterViewInit {
  @ViewChild('flexContentWrapper') element!: ElementRef;

  constructor() {}

  ngAfterViewInit(): void {}
}
