/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { FlexContentWrapperDirective } from './flex-content-wrapper.directive';

describe('Directive: FlexContentWrapper', () => {
  it('should create an instance', () => {
    const directive = new FlexContentWrapperDirective();
    expect(directive).toBeTruthy();
  });
});
