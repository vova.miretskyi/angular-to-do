export const ROOT = '/';

export const PROFILES_API_ROUTE = `${ROOT}profiles`;

export const TODOS_API_ROUTE = `${ROOT}todos`;
