export type TODO = {
  id: string;
  title: string;
  description: string;
};
