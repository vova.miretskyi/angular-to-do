import { CommonModule } from '@angular/common';
import { TodoPageComponent } from './todo-page/todo-page.component';
import { RouterModule, Routes } from '@angular/router';
import { TodoComponent } from './todo/todo.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TodosService } from './services/todos.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CreateTodoComponent } from './create-todo/create-todo.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: TodoPageComponent,
      },
      // {
      //   path: ':id',
      //   component: PokemonTemplateFormComponent,
      // },
    ],
  },
];

@NgModule({
  declarations: [TodoPageComponent, TodoComponent, CreateTodoComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [TodoPageComponent, TodoComponent, CreateTodoComponent],
  providers: [TodosService],
})
export class TodosModule {}
