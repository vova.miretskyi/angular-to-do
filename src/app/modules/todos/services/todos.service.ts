import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, tap } from 'rxjs';
import { TODOS_API_ROUTE } from 'src/app/utils/api-routes';
import { TODO } from '../models/todo';

const TODOS_API = `http://localhost:3000${TODOS_API_ROUTE}`;

@Injectable({
  providedIn: 'root',
})
export class TodosService {
  constructor(private http: HttpClient) {}

  private _refreshNeeded$ = new Subject<void>();

  get refreshNeeded$() {
    return this._refreshNeeded$.asObservable();
  }

  getTodos(): Observable<TODO[]> {
    return this.http.get<TODO[]>(TODOS_API);
  }

  addTodo(todoData: TODO): Observable<TODO> {
    return this.http.post<TODO>(TODOS_API, todoData).pipe(
      tap(() => {
        this._refreshNeeded$.next();
      })
    );
  }

  updateTodo(todoData: TODO): Observable<TODO> {
    return this.http.put<TODO>(`${TODOS_API}/${todoData.id}`, todoData);
  }

  removeTodo(todoId: TODO['id']): Observable<TODO> {
    return this.http.delete<TODO>(`${TODOS_API}/${todoId}`).pipe(
      tap(() => {
        this._refreshNeeded$.next();
      })
    );
  }
}
