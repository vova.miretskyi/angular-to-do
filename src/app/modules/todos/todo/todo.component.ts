import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TODO } from '../models/todo';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
})
export class TodoComponent {
  @Input() todo!: TODO;
  @Input() index: number = 0;
  @Output() update: EventEmitter<TODO> = new EventEmitter();
  @Output() remove: EventEmitter<TODO['id']> = new EventEmitter();

  isEditMode: boolean = false;

  toggleEditMode(active: boolean) {
    this.isEditMode = active;
  }

  updateTodo() {
    this.update.emit(this.todo);
    this.toggleEditMode(false);
  }

  removeTodo() {
    this.remove.emit(this.todo.id);
    this.toggleEditMode(false);
  }
}
