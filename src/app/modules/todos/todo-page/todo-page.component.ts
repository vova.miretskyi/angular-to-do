import { Component, OnDestroy, OnInit } from '@angular/core';
import { TODO } from '../models/todo';
import { TodosService } from '../services/todos.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.scss'],
})
export class TodoPageComponent implements OnInit, OnDestroy {
  todos!: TODO[];
  isCreateTodoModeActive: boolean = false;

  getAllTodosSubscription!: Subscription;
  addTodoSubscription!: Subscription;
  updateTodoSubscription!: Subscription;
  removeTodoSubscription!: Subscription;

  constructor(private todosService: TodosService) {}

  private getAllTodos() {
    this.getAllTodosSubscription = this.todosService
      .getTodos()
      .subscribe((data) => (this.todos = data));
  }

  toggleCreateTodoMode(isActive: boolean) {
    this.isCreateTodoModeActive = isActive;
  }

  ngOnInit() {
    this.todosService.refreshNeeded$.subscribe(() => {
      this.getAllTodos();
    });
    this.getAllTodos();
  }

  addTodo(event: TODO) {
    this.addTodoSubscription = this.todosService.addTodo(event).subscribe();
  }

  updateTodo(event: TODO) {
    this.updateTodoSubscription = this.todosService
      .updateTodo(event)
      .subscribe();
  }

  removeTodo(event: TODO['id']) {
    this.removeTodoSubscription = this.todosService
      .removeTodo(event)
      .subscribe();
  }

  ngOnDestroy(): void {
    this.getAllTodosSubscription.unsubscribe();
    this.addTodoSubscription.unsubscribe();
    this.updateTodoSubscription.unsubscribe();
    this.removeTodoSubscription.unsubscribe();
  }
}
