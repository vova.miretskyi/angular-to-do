import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';
import { TODO } from '../models/todo';

@Component({
  selector: 'create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.scss'],
})
export class CreateTodoComponent {
  @Output() add: EventEmitter<TODO> = new EventEmitter();

  private initialValues: TODO = {
    id: '',
    title: '',
    description: '',
  };

  todoForm: FormGroup<{ title: FormControl; description: FormControl }> =
    new FormGroup({
      title: new FormControl(this.initialValues.title, [
        Validators.minLength(5),
        Validators.required,
      ]),
      description: new FormControl(this.initialValues.description, [
        Validators.minLength(5),
      ]),
    });

  private onReset() {
    this.todoForm.reset();
  }

  onSubmit(event: Event) {
    event.preventDefault();

    if (this.todoForm.invalid) return;

    const params: TODO = {
      id: uuidv4(),
      title: this.todoForm.value.title,
      description: this.todoForm.value.description,
    };

    this.add.emit(params);

    this.onReset();
  }
}
