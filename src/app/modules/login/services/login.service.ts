import { Injectable } from '@angular/core';
import { Profile } from '../models/profile';
import { HttpClient } from '@angular/common/http';
import { Observable, lastValueFrom } from 'rxjs';
import { PROFILES_API_ROUTE } from 'src/app/utils/api-routes';

const PROFILES_API = `http://localhost:3000${PROFILES_API_ROUTE}`;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  profile?: Profile;

  constructor(protected http: HttpClient) {}

  private getProfile(profileEmail: Profile['email']): Observable<Profile[]> {
    return this.http.get<Profile[]>(`${PROFILES_API}?email=${profileEmail}`);
  }

  private validatePassword(password: Profile['password']) {
    return this.profile?.password === password;
  }

  private logInError(label: 'Email' | 'Password') {
    let error = 'Password is incorrect';

    if (label === 'Email') error = `User with this email doesn't exist`;

    return error;
  }

  async login({ email, password }: Profile) {
    await lastValueFrom(this.getProfile(email)).then(
      (data) => (this.profile = data[0])
    );

    if (!this.profile)
      return { loggedIn: false, error: this.logInError('Email') };

    if (!this.validatePassword(password))
      return { loggedIn: false, error: this.logInError('Password') };

    return {
      loggedIn: true,
      error: null,
    };
  }

  createProfile(values: Profile): Observable<Profile> {
    return this.http.post<Profile>(PROFILES_API, values);
  }
}
