import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page/login-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormComponent } from './login-form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from './services/login.service';

@NgModule({
  declarations: [LoginPageComponent, FormComponent],
  imports: [CommonModule, SharedModule, ReactiveFormsModule, HttpClientModule],
  exports: [LoginPageComponent, FormComponent],
  providers: [LoginService],
})
export class LoginModule {}
