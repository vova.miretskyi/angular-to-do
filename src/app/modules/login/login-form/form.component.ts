import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Profile } from '../models/profile';
import { Router } from '@angular/router';
import { TODOS_PAGE_ROUTE } from 'src/app/utils/routes';

@Component({
  selector: 'login-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent {
  values = {
    email: '',
    password: '',
  };

  isProfile: boolean = false;
  loginError: string | null = null;

  constructor(private loginService: LoginService, private router: Router) {}

  loginForm: FormGroup<{ email: FormControl; password: FormControl }> =
    new FormGroup({
      email: new FormControl(this.values.email, [
        Validators.email,
        Validators.required,
      ]),
      password: new FormControl(this.values.password, [
        Validators.minLength(4),
        Validators.required,
      ]),
    });

  private onReset() {
    this.loginForm.reset();
  }

  private async login({ email, password }: Profile) {
    const response = await this.loginService.login({
      email,
      password,
    });

    this.isProfile = response.loggedIn;
    this.loginError = response.error;

    if (!response.error) {
      localStorage.setItem('isLoggedIn', 'true');
    }
  }

  async onSubmit(event: Event) {
    event.preventDefault();

    if (this.loginForm.invalid) {
      return;
    }

    await this.login({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    });

    if (this.isProfile) {
      this.onReset();
      this.router.navigate([TODOS_PAGE_ROUTE]);
    }
  }
}
