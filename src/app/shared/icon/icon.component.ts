import { Component, Input } from '@angular/core';

type IconType = 'close' | 'edit' | 'submit' | 'plus' | 'minus';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent {
  @Input() type: IconType = 'close';

  cssClass = `icon`;
}
