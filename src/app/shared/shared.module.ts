import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { PageHocComponent } from './page-hoc/page-hoc.component';
import { FormComponentsModule } from './form-components/form-components.module';
import { IconComponent } from './icon/icon.component';

@NgModule({
  declarations: [PageHocComponent, NotFoundComponent, IconComponent],
  imports: [CommonModule],
  exports: [
    NotFoundComponent,
    PageHocComponent,
    FormComponentsModule,
    IconComponent,
  ],
})
export class SharedModule {}
