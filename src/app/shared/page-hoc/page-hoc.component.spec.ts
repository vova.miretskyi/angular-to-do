/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PageHocComponent } from './page-hoc.component';

describe('PageHocComponent', () => {
  let component: PageHocComponent;
  let fixture: ComponentFixture<PageHocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
