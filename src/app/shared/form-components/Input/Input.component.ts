import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'form-Input',
  templateUrl: './Input.component.html',
  styleUrls: ['./Input.component.scss'],
})
export class InputComponent implements OnInit {
  @Input() label: string = '';
  @Input() name: string = '';
  @Input() type: string = '';
  @Input() error?: string;

  constructor() {}

  ngOnInit() {}
}
