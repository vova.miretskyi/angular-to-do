export type FormInputProps = {
  name: string;
  label: string;
  type?: string;
  error?: string;
};
