import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './modules/login/login-page/login-page.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { LOGIN_PAGE_ROUTE, TODOS_PAGE_ROUTE } from './utils/routes';

const routes: Routes = [
  {
    path: LOGIN_PAGE_ROUTE,
    component: LoginPageComponent,
    pathMatch: 'full',
  },
  {
    path: TODOS_PAGE_ROUTE,
    loadChildren: () =>
      import('./modules/todos/todos.module').then((m) => m.TodosModule),
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
